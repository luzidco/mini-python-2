$(document).ready(function() {
    console.log("Allo, allo!")

    $.notifyDefaults({
        animate: {
            enter: 'animated fadeIn',
            exit: 'animated fadeOutUp'
        },
        allow_dismiss: false,
        delay: 3000,
        newest_on_top: true,
        placement: {
            align: 'center'
        }
    })
})
