$(document).ready(function() {
    $('#tasks').on('click', '.task-delete', function(event) {
        event.preventDefault()
        var task = $(event.target).closest('.task')
        var id = task.data('task-id')

        $.ajax(`/tasks/${id}`, {
            method: 'DELETE',
            success: function(data, status, xhr) {
                var id = $.parseJSON(xhr.responseText).id
                $(`#task-${id}`).remove()
                $.notifyAboutTasks(xhr)
            },
            error: function(xhr, status, error) {
                $.notifyAboutTasks(xhr)
            },
        })
    })
})
