$(document).ready(function() {
    var taskTemplate = _.template($('#task-tmpl').html())

    $('#task-new').closest('form').submit(function(event) {
        event.preventDefault()

        var input = $('#task-new')
        var name = input.val()

        $.ajax('/tasks/new', {
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                "name": name
            }),
            success: function(data, status, xhr) {
                var task = $.parseJSON(xhr.responseText).task
                var taskHTML = taskTemplate({ "task": task })
                $('#tasks').prepend(taskHTML)
                input.val("")
            },
            error: function(xhr, status, error) {
                $.notifyAboutTasks(xhr)
            }
        })
    })
})
