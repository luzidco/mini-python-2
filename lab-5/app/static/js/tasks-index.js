$(document).ready(function() {
    var taskTemplate = _.template($('#task-tmpl').html())
    var tasksList = $('#tasks')

    $.ajax('/tasks', {
        method: 'GET',
        success: function(data, status, xhr) {
            var tasks = $.parseJSON(xhr.responseText).tasks
            _(tasks).forEach(function(task) {
                var taskHTML = taskTemplate({ "task": task })
                tasksList.prepend(taskHTML)
            })
        },
        error: function(xhr, status, error) {
            $.notifyAboutTasks(xhr)
        }
    })
})
