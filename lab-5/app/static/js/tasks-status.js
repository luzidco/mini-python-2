$(document).ready(function() {
    $('#tasks').on('change', '.task-status', function(event) {
        event.preventDefault()

        var self = $(event.target)
        var task = self.closest('.task')
        var id = task.data('task-id')

        $.ajax(`/tasks/${id}/status`, {
            method: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify({
                // Event is triggered on the precise moment that the user clicks the input tag. So, while the read value
                // should be negated, it's already negated when being read here.
                "status": self.prop('checked')
            }),
            success: function(data, status, xhr) {
                var isdone = $.parseJSON(xhr.responseText).status
                if (isdone) {
                    self.prop('checked', true)
                    self.attr('checked', true) // Not really necessary, but it's to see the change in DOM as well.
                    task.addClass('task-isdone')
                } else {
                    self.prop('checked', false)
                    self.removeAttr('checked')
                    task.removeClass('task-isdone')
                }
            },
            error: function(xhr, status, error) {
                $.notifyAboutTasks(xhr)
            },
        })
    })
})
