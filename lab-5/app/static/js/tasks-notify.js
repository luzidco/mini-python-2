(function($) {
    $.notifyAboutTasks = function(xhr) {
        var notify = $.parseJSON(xhr.responseText).notify
        $.notify({
            "message": notify.message
        }, {
            "type": notify.type
        })
    }
})(jQuery)
