#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bottle import (
    delete,
    get,
    post,
    put,
    request,
    response,
    run,
    static_file,
)

from models.task import Task
from models.tasks import Tasks


@get('/tasks')
def index():
    if tasks:
        response.status = 200
        return {
            "success": True,
            "tasks": tasks.__json__(),
        }
    else:
        response.status = 500
        return {
            "success": False,
            "notify": {
                "type": "danger",
                "message": u"Wystąpił okropny i straszliwy błąd podczas wczytywania zadań. Ale u mnie działa. 😰"
            },
        }


@post('/tasks/new')
def new():
    task = tasks.add(Task(tasks.next_id, request.json.get('name')))
    if task:
        response.status = 201
        return {
            "success": True,
            "task": task.__json__(),
        }
    else:
        response.status = 500
        return {
            "success": False,
            "notify": {
                "type": "danger",
                "message": u"Wystąpił błąd podczas tworzenia nowego zadania."
            },
        }


@put('/tasks/<task_id:int>/status')
def update(task_id):
    new_status = request.json.get('status')
    task = tasks.find(task_id)
    if task:
        task.status = new_status
        tasks.persist()
        response.status = 200
        return {
            "success": True,
            "status": task.status,
        }
    else:
        response.status = 404
        return {
            "success": False,
            "notify": {
                "type": "danger",
                "message": u"Wystąpił błąd podczas zmiany statusu zadania #{}.".format(task_id).encode('utf-8')
            },
        }


@delete('/tasks/<task_id:int>')
def delete(task_id):
    task = tasks.remove(task_id)
    if task:
        response.status = 200
        return {
            "success": True,
            "id": task_id,
            "notify": {
                "type": "success",
                "message": u"Usunięto zadanie #{}.".format(task_id).encode('utf-8')
            },
        }
    else:
        response.status = 404
        return {
            "success": False,
            "notify": {
                "type": "danger",
                "message": u"Wystąpił błąd podczas usuwania zadania #{}.".format(task_id).encode('utf-8')
            },
        }


@get('/')
def html():
    return static_file('index.html', root='app/static')


@get('/<filename:path>')
def static(filename):
    return static_file(filename, root='app/static')


if __name__ == "__main__":
    # # Create new dumpfile, with empty list of tasks
    # tasks = Tasks()
    #
    # # Create new dumpfile, with defined list of tasks
    # tasks = Tasks()
    # tasks_data = [
    #     (u"Naucz się Pythona", False),
    #     (u"Naucz się JavaScript", False),
    #     (u"Przeczytaj dokumentację `Bottle`", True),
    #     (u"Zażółć gęślą jaźń", False),
    #     (u"Zażółć gęślą jaźń", True),
    #     (u"Zrób pracę domową z piątych zajęć", True),
    #     (u"Zadanie do usunięcia #1", False),
    #     (u"Pchnąć w tę łódź jeża lub ośm skrzyń fig.", False),
    #     (u"Zadanie do usunięcia #2", True),
    # ]
    # for task_data in tasks_data:
    #     tasks.add(Task(tasks.next_id, task_data[0], task_data[1]))
    #
    # # Load predefined dumpfile
    # tasks = Tasks.from_seed()
    #
    # # Load current dumpfile
    # tasks = Tasks.from_db()
    #
    tasks = Tasks.from_db()
    run(host='localhost', port=8080, debug=True, reloader=True)
