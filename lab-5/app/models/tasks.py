#!/usr/bin/env python
# -*- coding: utf-8 -*-

from itertools import count

from tasks_store import TasksStore


class Tasks(object):
    def __init__(self, store=TasksStore()):
        self._store = store
        self._tasks = store.load()
        try:
            self._uid = count(max(self._tasks.keys()) + 1)
        except:
            self._uid = count(1)

    def __repr__(self):
        return u"<{} {}>".format(self.__class__.__name__, repr(self._tasks.values()).decode('utf-8')).encode('utf-8')

    def __json__(self):
        return [task.__json__() for task in self._tasks.values()]

    @classmethod
    def from_db(cls):
        return cls(TasksStore.from_db())

    @classmethod
    def from_seed(cls):
        return cls(TasksStore.from_seed())

    def add(self, task):
        self._tasks[task.id] = task
        saved = self.persist()
        if not saved:
            task = None
        return task

    def find(self, task_id):
        return self._tasks.get(task_id, None)

    def remove(self, task_id):
        task = None
        try:
            task = self._tasks.pop(task_id)
        except KeyError as err:
            print(err)
        saved = self.persist()
        if not saved:
            task = None
        return task

    @property
    def next_id(self):
        return self._uid.next()

    def persist(self):
        return self._store.save(self._tasks)
