#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cPickle as pickle
import errno
from sys import exit


class TasksStore(object):
    DBPATH = 'tasks.dump'
    SEEDPATH = 'tasks.seed'

    def __init__(self, dbpath=None):
        self._dbpath = dbpath

    def save(self, tasks):
        saved = False
        try:
            with open(self._dbpath, 'w') as db:
                pickler = pickle.Pickler(db, protocol=pickle.HIGHEST_PROTOCOL)
                pickler.dump(tasks)
                saved = True
        except Exception as err:
            print(u"Could not save a dump {} because {}.".format(self._dbpath, err)).encode('utf-8')
            pass
        return saved

    def load(self):
        tasks = dict()
        if self._dbpath is None:
            self._create_db(self.DBPATH)
        else:
            tasks = self._load_db(self._dbpath)
        return tasks

    @classmethod
    def from_db(cls):
        return cls(cls.DBPATH)

    @classmethod
    def from_seed(cls):
        store = cls(cls.SEEDPATH)
        tasks = store.load()
        store._dbpath = cls.DBPATH
        store.save(tasks)
        return store

    def _create_db(self, dbpath):
        db = None
        try:
            db = open(dbpath, 'w')
        except IOError as err:
            print(u"Could not create new dump {} because {}. Aborting...".format(dbpath, err.errno).encode('utf-8'))
            exit(-1)
        finally:
            if db is not None:
                db.close()
        self._dbpath = dbpath

    def _load_db(self, dbpath):
        tasks = dict()
        db = None
        try:
            db = open(dbpath, 'r')
        except IOError as err:
            if err.errno is errno.ENOENT:
                print(u"Could not find dumpfile {}. Creating now...".format(self._dbpath))
                self._create_db(dbpath)
                db = open(dbpath, 'r')
            else:
                print(u"Could not read dump {} because {}. Aborting...".format(dbpath, err.errno).encode('utf-8'))
                exit(-1)
        try:
            tasks = pickle.Unpickler(db).load()
        except Exception as err:
            print(u"Could not unpickle {} because {}. Aborting...".format(self._dbpath, err))
            exit(-1)
        db.close()
        return tasks
