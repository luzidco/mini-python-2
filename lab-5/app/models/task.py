#!/usr/bin/env python
# -*- coding: utf-8 -*-

from cgi import escape


class Task(object):
    def __init__(self, id, name, status=False):
        self._id = id
        self._name = unicode(name)
        self._status = status

    def __repr__(self):
        return u"<{} #{} \"{}\" {}>".format(self.__class__.__name__, self.id, self.name, self.isdone).encode('utf-8')

    def __json__(self):
        return {
            "id": self.id,
            "name": escape(self.name),
            "status": self.status,
            "isdone": self.isdone,
        }

    @property
    def id(self):
        return self._id

    @property
    def name(self):
        return self._name

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, new_status):
        self._status = bool(new_status)
        return self._status

    @property
    def isdone(self):
        return bool(self._status) is True
