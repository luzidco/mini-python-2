### Użycie

    $ make bootstrap
    $ make run

Jak zacząć serwer z własną listą zadań zostało opisane na końcu pliku `app/app.py`.

### Praca domowa:

- Numery zadań zostały schowane z UI, są przechowywane w pomocnicznym atrybucie `data-task-id` w elementach `li.task`.
- Podczas zrzucania danych przez `pickle`, jest używany najwyższy protokół `pickle.HIGHEST_PROTOCOL`.
- Serwer nie odpowiada stronami błędów, natomiast w przypadku błędów zwraca odpowiedni do sytuacji status HTTP, oraz wyświetlany jest komunikat o błędzie na stronie.
- CSS i wzór strony został zaczerpnięty z projektu [TodoMVC](http://todomvc.com/).
- Wszystkie pliki statyczne `*.css`, `*.js` i sam `index.html` są serwowane przez Bottle.
- Serwer nie generuje dokumentu HTML. `index.html` jest serwowany jako plik statyczny, natomiast wszystkie akcje związanie z zadaniami są przesyłanie za pomocą `XMLHttpRequest`.
- Wszystkie zapytania generowane przez stronę są wykonywane asynchronicznie. Są skierowane do endpointów wystawianych przez serwer Bottle, które mniej lub bardziej mają przypominać REST-owo udostępniany zasób zadań. Wszystkie endpointy zwracają dane typu `application/json` lub puste odpowiedzi.
    - `GET /tasks` zwraca listę wszystkich zadań;
    - `POST /tasks` przyjmuje JSON `{ "name": <string> }`, tworzy zadanie o treści podanej w `"name"` i zwraca JSON z nowo-utworzonym zadaniem;
    - `PUT /tasks/:id/status` przyjmuje JSON `{ "status": <boolean> }`, zmienia status zadania o id z adresu na nowy, podany status i zwraca aktualny status zadania na serwerze;
    - `DELETE /tasks/:id` usuwa z listy zadań, zadanie o podanym id i zwraca jako potwierdzenie id usuniętego zadania.

### Uwagi do kodu:

- W klasach `Task`, `Tasks` i `TasksStore` starałem rozdzielić logikę odpowiednio na sam obiekt danych, zarządzanie kolekcją danych i trwałe zapisywanie danych. Wydaje mi się, że ze stosunkowo dobrym efektem udało mi się rozdzielić te funkcjonalności.
- Prawdopodobnie powinienem jeszcze wydzielić logikę reprezentacji danych `Task` i kolekcji `Tasks` do JSON. Uznałem, że ich metody `__json__()` były zbyt proste, by miało to sens.
- Sporo kodu w części JavaScriptowej jest bardzo podobne do siebie i na pewno można je w znacznym stopniu zrefaktoryzować. Ponieważ dogłębna nauka JavaScript i jQuery nie była celem tej pracy domowej, pominąłem ten krok.
- Pliki statyczne serwowane przez Bottle mogłyby być połączone i serwowane jako jeden plik.
