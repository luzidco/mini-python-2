#!/usr/bin/env python
# -*- coding: utf-8 -*-

from helpers import path_to
from nose.tools import assert_equals
from nose.tools import raises

from PIL import Image

from gallery.luminance import LumCalc
from gallery.luminance import LumRank
from gallery.photo import Photo


def _calc_luminance(filepath):
    return LumCalc(Image.open(filepath)).luminance


def test_lum_calc_on_pure_white():
    luminance = _calc_luminance(path_to('valid/white.png'))
    assert_equals(255, luminance)


def test_lum_calc_on_pure_black():
    luminance = _calc_luminance(path_to('valid/black.png'))
    assert_equals(0, luminance)


def test_lum_calc_on_a_photo():
    photo = Photo(path_to('valid/photo.jpg'))
    assert_equals(124, photo.luminance)


def test_lum_category_constructor():
    assert_equals("<LumRank Unmatched (-1, -1)>", repr(LumRank.Unmatched))


def test_lum_category_generator():
    n = 13
    names = [str(i + 1) for i in xrange(n)]
    categories = LumRank.generate(names)
    # import ipdb; ipdb.set_trace()
    assert_equals(n, len(categories))
    assert_equals("<LumRank 1 (237, 255)>", repr(categories[0]))
    assert_equals("<LumRank 12 (21, 40)>", repr(categories[11]))
    assert_equals("<LumRank 13 (0, 20)>", repr(categories[12]))


@raises(ValueError)
def test_lum_category_generator_with_invalid_categories_number():
    LumRank.generate([])
