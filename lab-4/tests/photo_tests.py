#!/usr/bin/env python
# -*- coding: utf-8 -*-

from nose.tools import assert_equals

import os
from timeit import default_timer as timer

from gallery.photo import Photo
from helpers import path_to


def test_photo_constructor():
    photo = Photo(path_to("valid/photo.jpg"))
    assert_equals(
        "<Photo tests/pictures/valid/photo.jpg>",
        repr(photo))


def test_photo_filepath():
    photo = Photo(path_to("valid/photo.jpg"))
    assert_equals('tests/pictures/valid/photo.jpg', os.path.relpath(photo.filepath))


def test_photo_filerelpath():
    photo = Photo(path_to("valid/photo.jpg"))
    assert_equals('pictures/valid/photo.jpg', photo.filerelpath('./tests'))


def test_photo_luminance():
    white = Photo(path_to("valid/white.png"))
    black = Photo(path_to("valid/black.png"))
    assert_equals(255, white.luminance)
    assert_equals(0, black.luminance)


def test_photo_calculations_caching():
    photo = Photo(path_to("valid/photo.jpg"))
    times = []
    for i in xrange(2):
        start = timer()
        photo.luminance
        end = timer()
        times.append(end - start)
    assert times[1] < times[0]


def test_photo_thumbnail():
    photo = Photo(path_to("valid/photo.jpg"))
    thumb = photo.thumbnail
    assert_equals(
        "<Photo tests/pictures/valid/photo.thumb.jpg>",
        repr(thumb))
    os.remove(thumb.filepath)
