#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os


def path_to(filepath):
    return os.path.abspath(os.path.join("tests/pictures", filepath))
