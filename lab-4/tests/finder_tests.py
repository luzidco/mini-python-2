#!/usr/bin/env python
# -*- coding: utf-8 -*-

from nose.tools import assert_equals
from helpers import path_to

from gallery.finder import Finder


def test_finder_on_valid_photos():
    finder = Finder(path_to("valid/"))
    assert_equals(4, sum(1 for imagepath in finder))


def test_finder_on_invalid_photos():
    finder = Finder(path_to("invalid/"))
    assert_equals(0, sum(1 for imagepath in finder))
