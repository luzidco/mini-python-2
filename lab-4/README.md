### Uruchomienie

    $ make bootstrap
    $ make run

### Praca domowa:

Parę zmian takie jak podział na klasy, własny dekorator, zabawy argumentami lub nadmierne wykorzystywanie list-comprehension są zupełnie na wyrost dla tak prostego projektu. Chciałem się pobawić Pythonem i poznać się przy okazji specyficzne dla Pythona elementy składni.

Projekt nie działa szybko (ok. 25s na 1.4GHz Intel Core i5), ponieważ kopiuje obrazy do galerii, generuje miniaturki i korzysta z zewnętrznego templates engine do generowania dokumentu HTML. I przetwarza większy zbiór, większych obrazów. Wybrałem napisanie wolniejszego, lecz czytelniejszego kodu nad kodem *szybkim*. Lwia część czasu pracy i tak zależy od wyliczania jasności i tworzenia miniatur, nad którymi nie mam bezpośredniej kontroli.

- Kontrola poprawności przekazywanych argumentów jest ograniczona, i ma miejsce tylko w klasie `Gallery`, która jako jedyna powinna być używana przez użytkowników.
- Projekt przeszedł refactoring, głównie skupiając się na rozdzieleniu logiki pomiędzy klasy z zachowaniem [`SRP`](https://en.wikipedia.org/wiki/Single_responsibility_principle).
- Stworzono generator, z użyciem `yield`, w klasie `Finder` odpowiedzialnej za wyszukiwanie plików obrazów. `Finder` wczytyje tylko prawidłowe pliki obrazów obsługiwanych przez `pillow`.
- Metoda `SuperObrazek.brightness()` (aktualnie `Photo.luminance`) dostała mechanizm cache'owania zwracanej przez siebie wartości. Jej logika została wydzielona do zewnętrznej klasy.
- Dodanie kroku zaokrąglenia wyliczanej wartości jasności względnej zdjęcia pozwala na prawidłowe uzyskanie pełnego zakresu wartości od `0` do `255`.
- Do generowania pliku HTML używam silnika template'ów [`jinja2`](http://jinja.pocoo.org/docs/).
- Generowany dokument HTML:
    - jest oparty na [HTML 5 Boilerplate](https://html5boilerplate.com/),
    - ładuje framework [Bootstrap](http://getbootstrap.com/) do unormowania podstawowych stylów oraz stworzenia responsywnego grida,
    - ładuje bibliotekę [masonry](http://masonry.desandro.com/) do automatycznego ułożenia miniatur zdjęć,
    - i dodatkowo [Lightbox for Bootstrap](http://ashleydw.github.io/lightbox/) do wyświetlania modala z oryginalnym zdjęciem.
- W dokumencie nie są stosowane najlepsze praktyki w paru przypadkach; nie są cache'owane zewnętrzne style i skrypty, sktruktura dokumentu i tak przechowuje wszystkie zdjęcia w pamięci zamiast ładować je na żądanie, nawet gdy nie są wyświetlane, a część skryptów i stylów jest wpisana bezpośrednio w dokument, zamiast do oddzielnych plików. Błędy te zostały świadomie popełnione. Tworzenie stron WWW nie była celem tych warsztatów.
- Problem z kodowaniem każdego stworzonego stringa do `UTF-8`. Szczerze mówiąc nie znalazłem sposoby jak to elegancko rozwiązać, poza przejściem na Pythona 3.
