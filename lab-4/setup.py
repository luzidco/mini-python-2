from setuptools import setup

config = {
    'name': 'gallery',
    'version': '1.0',
    'description': 'Daftcode 4th Workshop',
    'url': 'https://gitlab.com/luzidco/mini-python-2/tree/master/lab-4',
    'author': 'Tomasz Cudziło',
    'author_email': 'tomasz@luzid.co',
	'license': 'MIT',
    'packages': ['gallery'],
    'install_requires': ['pillow'],
}
setup(**config)
