#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import defaultdict
import weakref


class RefsTracker(object):
    __refs__ = defaultdict(list)

    def __init__(self):
        self.__class__.__refs__[self.__class__].append(weakref.ref(self))

    @classmethod
    def refs(cls):
        refs = []
        for ref in [weak_ref() for weak_ref in cls.__refs__[cls]]:
            if ref is not None:
                refs.append(ref)
        return refs
