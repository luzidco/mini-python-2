#!/usr/bin/env python
# -*- coding: utf-8 -*-

import errno
from itertools import groupby
from jinja2 import Environment
from jinja2 import PackageLoader
import os
import random
import shutil

from finder import Finder
from luminance import LumRank
from photo import Photo


class HTMLGenerator(object):
    OUTPUT_PHOTOS_SUBDIR = 'pictures'
    OUTPUT_FILENAME = 'index.html'
    TEMPLATE_DIR = ['gallery', 'template']
    TEMPLATE_FILENAME = 'index.html.jinja2'

    def __init__(self, template_dir=TEMPLATE_DIR, template_filename=TEMPLATE_FILENAME):
        self._template_dir = template_dir
        self._template_filename = template_filename

    def generate(self, gallery_model, photos_input_dir, output_dir):
        self.output_dir = self._create_output_dir(output_dir)
        self.photos_output_dir = self._copy_photos(photos_input_dir, self.output_dir)
        self.ranks, self.ranked_photos = gallery_model.populate(self.photos_output_dir)
        self._generate_html()
        return self.output_dir

    def _create_output_dir(self, output_dir):
        try:
            os.makedirs(output_dir)
        except OSError as err:
            if err.errno != errno.EEXIST:
                raise
        return output_dir

    def _copy_photos(self, input_dir, output_dir):
        photos_input_dir = input_dir
        photos_output_dir = os.path.join(output_dir, self.OUTPUT_PHOTOS_SUBDIR)
        if (os.path.exists(photos_output_dir)):
            shutil.rmtree(photos_output_dir)
        shutil.copytree(photos_input_dir, photos_output_dir)
        return photos_output_dir

    def _generate_html(self):
        env = Environment(loader=PackageLoader(*self._template_dir))
        template = env.get_template(self._template_filename)
        html = template.render(ranks=self.ranks,
                               ranked_photos=self.ranked_photos,
                               site_root=self.output_dir).encode('utf-8')
        html_path = os.path.join(self.output_dir, self.OUTPUT_FILENAME)
        with open(html_path, 'w+') as html_file:
            html_file.write(html)


class GalleryModel(object):
    def __init__(self, ranks_names):
        self.ranks = LumRank.generate(ranks_names)

    def populate(self, photos_dir):
        self.photos = self._load_photos(photos_dir)
        self.ranked_photos = self._rank_photos()
        return self.ranks, self.ranked_photos

    def _load_photos(self, photos_dir):
        return [Photo(imagepath) for imagepath in Finder(photos_dir)]

    def _rank_photos(self):
        photos = sorted(self.photos,
                        cmp=lambda rl, rr: cmp(rl.lower, rr.lower),
                        key=self._match_rank)
        ranked_photos = dict()
        for rank, photos_for_rank in groupby(photos, self._match_rank):
            ranked_photos[rank] = list(photos_for_rank)
            random.shuffle(ranked_photos[rank])
        return ranked_photos

    def _match_rank(self, photo):
        return LumRank.match(self.ranks, photo)
