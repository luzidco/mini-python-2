#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

from PIL import Image

from luminance import LumCalc


class Photo(object):
    THUMBNAIL_SIZE = (320, 320)
    THUMBNAIL_SUFFIX = '.thumb'

    def __init__(self, filepath):
        self._filepath = os.path.abspath(filepath)
        self._image = Image.open(self.filepath).convert('RGB')
        self._thumbnail = None
        self._luminance = None

    def __repr__(self):
        return u"<{} {}>".format(self.__class__.__name__, os.path.relpath(self._filepath)).encode('utf-8')

    @property
    def filepath(self):
        return self._filepath

    def filerelpath(self, root):
        return os.path.relpath(self._filepath, root)

    @property
    def luminance(self):
        if self._luminance:
            return self._luminance
        self._luminance = LumCalc(self._image).luminance
        return self._luminance

    @property
    def thumbnail(self):
        if self._thumbnail:
            return self._thumbnail
        thumbbase, thumbext = os.path.splitext(self._filepath)
        thumbpath = "{}{}{}".format(thumbbase, self.THUMBNAIL_SUFFIX, thumbext)
        thumbnail = self._image.copy()
        thumbnail.thumbnail(self.THUMBNAIL_SIZE)
        thumbnail.save(thumbpath)
        self._thumbnail = self.__class__(thumbpath)
        return self._thumbnail
