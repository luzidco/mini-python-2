#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

from html import GalleryModel
from html import HTMLGenerator


class Gallery(object):
    def __init__(self,
                 input_dir='./pictures',
                 output_dir='./output',
                 ranks_names=[u"Jasne", u"Średnio-Jasne", u"Średnio-Ciemne", u"Ciemne"]):
        if not os.path.exists(input_dir):
            print(u"Input directory `{}` cannot be read.".format(input_dir).encode('utf-u'))
            sys.exit(-1)
        self._input_dir = os.path.abspath(os.path.normpath(input_dir))
        self._output_dir = os.path.abspath(os.path.normpath(output_dir))
        self._model = GalleryModel(ranks_names)
        self._html = HTMLGenerator()

    def generate(self):
        self._html.generate(self._model, self._input_dir, self._output_dir)


if __name__ == "__main__":
    gallery = Gallery()
    gallery.generate()
