#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PIL import ImageStat
from slugify import slugify


class LumCalc(object):
    # Vide: https://en.wikipedia.org/wiki/Relative_luminance
    __coeffs__ = [0.2126, 0.7152, 0.0722]

    def __init__(self, image):
        self.image = image

    @property
    def luminance(self):
        colors = ImageStat.Stat(self.image).mean
        coeffs = self.__coeffs__
        luminance = sum(coeff * color for coeff, color in zip(coeffs, colors))
        return int(round(luminance))


class LumRank(object):
    MIN_LUM = 0
    MAX_LUM = 255

    def __init__(self, name, thresholds):
        self._name = unicode(name)
        self._id = u"rank-{}".format(slugify(self._name)).encode('utf-8')
        self._thresholds = thresholds

    def __repr__(self):
        return u"<{} {} {}>".format(self.__class__.__name__, self._name, self._thresholds).encode('utf-8')

    @property
    def name(self):
        return self._name

    @property
    def id(self):
        return self._id

    @property
    def lower(self):
        return self._thresholds[0]

    @property
    def upper(self):
        return self._thresholds[1]

    def matches(self, photo):
        return (
            photo.luminance >= self.lower and
            photo.luminance <= self.upper
        )

    @classmethod
    def match(cls, ranks, photo):
        for rank in ranks:
            if rank.matches(photo):
                return rank
        return cls.Unmatched  # Because it's better than returning `None`.

    @classmethod
    def generate(cls, names):
        n = len(names)
        if n < 1 or n > 255:
            raise ValueError
        # These manual assignments are there to ensure that whole spectrum is
        # covered (even if slightly not uniformly).
        steps = [255 - i * (cls.MAX_LUM - cls.MIN_LUM) // n for i in xrange(n + 1)]
        steps[0] = cls.MAX_LUM
        steps[n] = cls.MIN_LUM - 1
        ranks = list()
        for name, upper, lower in [(names[i], steps[i], steps[i + 1] + 1) for i in xrange(n)]:
            ranks.append(cls(name, (lower, upper)))
        return ranks

LumRank.Unmatched = LumRank(u"Unmatched", (-1, -1))
