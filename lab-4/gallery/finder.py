#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from PIL import Image


class Finder(object):
    def __init__(self, path):
        self._path = os.path.abspath(path)

    def __iter__(self):
        for directory, subdirectories, filenames in os.walk(self._path):
            for filename in filenames:
                filepath = os.path.join(directory, filename)
                try:
                    Image.open(filepath).verify()
                except (SyntaxError, IOError):
                    continue
                # Didn't want to return inside the `try` block.
                yield filepath
