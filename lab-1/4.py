#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

def divisible(start, stop, divisor = 2):
    for i in xrange(start, stop):
        if i % divisor == 0:
            print i

if __name__ == "__main__":
    divisible(*[int(i) for i in sys.argv[1:]])
