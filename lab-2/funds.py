#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from itertools import ifilter, imap

from bs4 import BeautifulSoup, SoupStrainer, Tag
import requests
import unicodecsv


INPUT = 'http://www.bankier.pl/fundusze/notowania/wszystkie'
OUTPUT = 'funds.csv'


def _is_a_tag(cell):
    return isinstance(cell, Tag)


def _extract_header(cell):
    HEADER_CUTOUT = '  A D'
    return cell.get_text(' ').split(HEADER_CUTOUT)[0]


def _extract_headers(row):
    return map(_extract_header, ifilter(_is_a_tag, row.children))


def _extract_value(cell):
    if cell.has_attr('data-value'):
        return cell['data-value']
    else:
        text = cell.text.strip()
        if not cell.get('class')[0] == 'colTicker':
            text = text.replace(u'\xa0', '')  # Remove the leftover &nbsp; HTML entities
        return text


def _extract_values(rows):
    return (imap(_extract_value, ifilter(_is_a_tag, row.children)) for row in rows)


def scrape(url):
    IGNORED_SELECTORS = ['tr.staticRow']
    document = requests.get(url)
    document.encoding = 'utf-8'
    html = document.text
    # Read and parse only the relevant part of the HTML document
    soup = BeautifulSoup(html, 'lxml', parse_only=SoupStrainer('tr'), from_encoding='utf-8')
    # Get rid of the empty rows
    for ignored_selector in IGNORED_SELECTORS:
        for empty_row in soup.select(ignored_selector):
            empty_row.decompose()
    # Prepare the iterator chain
    all_rows = soup.children
    # Discard the first child of BeautifulSoup-parsed document
    doctype = unicode(next(all_rows))
    # Manually load and read the table's headers
    headers = _extract_headers(next(all_rows))
    # Create a chain of utitility iterators reading values from the remaining <tr> tags
    values = _extract_values(all_rows)
    return doctype, headers, values


def print_to_csv(filename, headers, rows):
    csv_file = open(filename, 'w+')
    csv_writer = unicodecsv.writer(csv_file, encoding='utf-8', delimiter=';')
    csv_writer.writerow(headers)
    csv_writer.writerows(rows)
    csv_file.close()


if __name__ == '__main__':
    doctype, headers, rows = scrape(INPUT)
    print_to_csv(OUTPUT, headers, rows)
