### Użycie

    $ make bootstrap
    $ make run

### Praca domowa

- Udało się uzyskać ok. 38% krótszy czas pracy skryptu po następujących zmianach:
    - Używam parsera `lxml`. Jest napisany w kodzie natywnym, co znacząco wpływa na jego wydajność.
    - Podczas tworzenia obiektu `BeautifulSoup`, przekazuję do konstruktora obiekt `SoupStrainer`, który powoduje parsowanie tylko wybranej części ściągniętego dokumentu HTML.
    - Narzucam z góry kodowanie znaków parsowanego dokumentu, co pozwala na pominięcie etapu zgadywania kodowania.
    - W żadnym momencie działania skryptu, nie jest przetrzymywana w pamięci pełna lista sprasowanych wartości. Jest to uzyskane poprzez stworzenie pipeline'u iteratorów iterujących leniwie po elementach dokumentu sprasowanego przez `BeautifulSoup`.
- Nie udało mi się zmniejszyć ilości kodu; wręcz przeciwnie jest go więcej. Natomiast, na pewno, jest on bardziej czytelny.
- Problem pustych linii został rozwiązany przez usunięcie, z drzewa `BeautifulSoup`, elementów z selektorem `.staticRow`. Te elementy powodowały niepoprawne wpisy w pliku CSV.
- Wartości do ostatniej kolumny pliku CSV są odczytywane z atrybutu `data-value` ostanich komórek z parsowanej tabeli.
